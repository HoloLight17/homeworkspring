package ru.ithub.svyat.homework.services;

import lombok.Getter;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import ru.ithub.svyat.homework.models.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Getter
@Log
public class UserService {
    private final List<User> users = new ArrayList<>();

    public void addUser(User user) {
        if (users.stream().anyMatch(user1 -> Objects.equals(user1.getUsername(), user.getUsername()))) {
            log.severe("Ошибка при регистрации: имя пользователя занято");
        } else {
            users.add(user);
            log.info("Успешная регистрация");
        }
    }
}
